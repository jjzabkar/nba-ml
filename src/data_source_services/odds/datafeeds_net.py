import requests

from src.cache import get_cache

DEFAULT_CACHE_TIMEOUT = 24 * 3600 * 365
cache = get_cache()

#  This API only includes high-speed feeds and therefore requires a business subscription.
# https://high-speed-live-sports-scores-basketball-nba.datafeeds.net/api/json/scores/v1/basketball/nba?month=12&year=2020&api-key=a2547dae8f90ecfa04854967fd52f79a


@cache.memoize(timeout=DEFAULT_CACHE_TIMEOUT)
def get_basketball_scores(month, year, api_key='a2547dae8f90ecfa04854967fd52f79a', result_size=1000):
    subresource = 'json/scores/v1/basketball/nba'
    return requests.get(url=f'https://high-speed-live-sports-scores-basketball-nba.datafeeds.net/api/{subresource}?month={month}&year={year}&api-key={api_key}&results={result_size}').json()


@cache.memoize(timeout=DEFAULT_CACHE_TIMEOUT)
def get_odds(api_key='c7b4a105979876aa41bf64255474e6fa', result_size=1000):
    """
    https://datafeeds.net/docs/bovada-sports-odds
    :return:
    """
    subresource = 'json/odds/v2/basketball/nba'
    result = requests.get(f'https://bovada-low-speed-basketball-nba.datafeeds.net/api/{subresource}?api-key={api_key}&results={result_size}')
    return result.json()


@cache.memoize(timeout=DEFAULT_CACHE_TIMEOUT)
def get_injuries(api_key = 'a2547dae8f90ecfa04854967fd52f79a', result_size=1000):
    subresource = 'json/injuries/v1/basketball/nba'
    result = requests.get(f'https://player-injuries.datafeeds.net/api/{subresource}?api-key={api_key}&results={result_size}')
    return result.json()


dec_2020 = get_basketball_scores(12, 2020)
odds = get_odds()
injuries = get_injuries()

print('done')