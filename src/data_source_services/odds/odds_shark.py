import json
import os

import dateutil.parser as dateparser
import requests
from bs4 import BeautifulSoup

from src.cache import CACHE_DIR


def get_team_ids_map():
    team_ids = []
    url = 'https://www.oddsshark.com/nba/game-logs'
    url = 'https://www.oddsshark.com/nba/extended-standings'
    response = requests.get(url=url)
    bs = BeautifulSoup(response.content, 'html.parser')
    tables = bs.select('table')
    for table in tables:
        for alink in table.select('tbody > tr > td > a'):
            team_id = alink.get('href').split('/')[-1]
            team_ids.append({
                'oddsshark_team_id': team_id,
                'team': alink.string})
    return team_ids


def get_oddsshark_team_game_log_html(team_id, year):
    file = f'{CACHE_DIR}/ats_values_for_teamid_{team_id}_{year}.html'
    if os.path.exists(file):
        print(f'\t load cached file: {file}')
        with open(file, 'r') as myfile:
            return myfile.read()
    else:
        url = f'https://www.oddsshark.com/stats/gamelog/basketball/nba/{team_id}/{year}'
        print(f'\tinvoked get_oddsshark_team_game_log_html({team_id},{year})\t url={url}')
        response = requests.get(url=url)
        with open(file, 'w') as f:
            f.write(str(response.content))
        return response.content


def get_abbrev(team_name):
    """
    map from oddsshark team names to basketball-reference standard abbrev's
    :param team_name:
    :return: standard basketball-reference abbrev
    """
    result = {
        'Atlanta':'ATL',
        'Boston':'BOS',
        'Brooklyn':'BRK',
        'Charlotte':'CHO',
        'Chicago':'CHI',
        'Cleveland':'CLE',
        'Dallas':'DAL',
        'Denver':'DEN',
        'Detroit':'DET',
        'Golden State':'GSW',
        'Houston':'HOU',
        'Indiana':'IND',
        'LA Clippers':'LAC',
        'LA Lakers':'LAL',
        'Memphis':'MEM',
        'Miami':'MIA',
        'Milwaukee':'MIL',
        'Minnesota':'MIN',
        'New York': 'NYK',
        'New Orleans':'NOP',
        "Oklahoma City":'OKC',
        'Orlando':'ORL',
        'Portland':'POR',
        'Philadelphia':'PHI',
        'Phoenix':'PHO',
        'Sacramento':'SAC',
        'San Antonio':'SAS',
        'Toronto':'TOR',
        'Utah':'UTA',
        'Washington':'WAS',

    }.get(team_name)
    assert team_name is not None, f'expected team_abbr for team {team_name}'
    return result


def get_oddsshark_ats_values(team_id, team_name, year):
    values = []
    print(f'invoked get_oddsshark_ats_values for {year} team {team_name} ({team_id})')
    get_abbrev(team_name) # validation
    html = get_oddsshark_team_game_log_html(team_id, year)
    soup = BeautifulSoup(html, 'html.parser')

    tables = soup.select('table')
    for table in tables:
        for tr in table.select('tbody > tr'):
            # [date, opponent, type, result, score, against_the_spread_result, spread, over_under, total_points
            tds = tr.select('td')
            if tds[4].contents[0] is None or tds[1].contents[2] is None:
                raise Exception('wtf yo')

            values.append({
                'oddsshark_team_id': team_id,
                'team': team_name,
                'abbrev': get_abbrev(team_name),
                'date_str': tds[0].string,
                'opponent': tds[1].contents[2].strip(),
                'opponent.abbrev': get_abbrev(tds[1].contents[2].strip()),
                'type': tds[2].string,
                'result': tds[3].string,
                'score_string': tds[4].contents[0].strip(),
                'against_the_spread_result': tds[5].string,
                'point_spread': float(tds[6].string),
                'over_under': tds[7].string,
                'total': float(tds[8].string)
            })
    return values


def get_ats_values_for_season(season):
    """
    :param season: the end year of the season  (e.g. "2020" for 2019-2020)
    :return:
    """
    print(f'get_ats_values_for_season({season})')
    file = f'{CACHE_DIR}/ats_values_for_season_{season}.json'
    if os.path.exists(file):
        with open(file, 'r') as myfile:
            print(f'loading file {file}', flush=True)
            data = myfile.read()
            print(f'converting to JSON', flush=True)
            obj = json.loads(data)
            print(f'loaded')
            return obj
    else:
        team_ids = get_team_ids_map()
        ats_values = {}
        for obj in team_ids:
            team_id, team = obj['oddsshark_team_id'], obj['team']
            team_abbr = get_abbrev(team)
            ats_values[team_abbr] = get_oddsshark_ats_values(team_id, team, season)

        with open(file, 'w') as f:
            json.dump(ats_values, f, indent=4, sort_keys=True)

        return ats_values


def lambda_lookup_points_spread_column(row, ats_values, abbrev, column_name='point_spread'):
    # found = False
    # row[abbrev]
    try:
        ats_value_list = ats_values[row[abbrev]]
    except KeyError as ke:
        print(f'wtf: unable to find ats_value for row[abbrev]={row[abbrev]}')
    assert len(ats_value_list) != 0, "expected ats_values"
    row_date = row['date']
    for ats in ats_value_list:
        ats_date = dateparser.parse(ats['date_str']).date()
        if ats_date == row_date:
            return ats[column_name]
    assert False, f'unable to find column_name={column_name} for {abbrev} game date {row_date} row={row}'
    return None




# SEASON = 2019
# ats_values = get_ats_values_for_season(SEASON)
# games_df = get_games_featurized_df(season=SEASON)
# ats_feature_df = get_points_spread_feature_df(df=games_df, season=SEASON)
#
# print(f'games_df.describe:\n{games_df.describe()}')
# print(f'ats_feature_df.describe:\n{ats_feature_df.describe()}')
# print(f'ats_feature_df.info:\n{ats_feature_df.info()}')
# print('done')

