import requests
from bs4 import BeautifulSoup

from src.cache import get_file_system_cache, DEFAULT_CACHE_TIMEOUT

cache = get_file_system_cache()


@cache.cached(timeout=DEFAULT_CACHE_TIMEOUT, key_prefix='betfirm/get_betfirm_moneyline_conversion')
def get_betfirm_moneyline_conversion():
    print('get_betfirm_moneyline_conversion()')
    url = 'https://www.betfirm.com/nba-point-spread-money-line-conversion-chart/'
    response = requests.get(url=url, headers=
    {
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_16_1) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/87.0.4280.140 Safari/537.35',
    })
    bs = BeautifulSoup(response.content, 'html.parser')
    table = bs.select('table')[0]
    moneylines_dict = {}
    for tr in table.select('tbody > tr'):
        tds = tr.select('td')
        line = float(tds[0].string)
        fav_ml = float(tds[1].string)
        dog_ml = float(tds[4].string)
        moneylines_dict[line] = {
            'fav_ml': fav_ml,
            'dog_ml': dog_ml,
        }

    return moneylines_dict


# result = get_betfirm_moneyline_conversion()
# result = get_betfirm_moneyline_conversion()
# print('done')
