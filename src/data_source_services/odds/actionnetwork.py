#https://www.actionnetwork.com/betting-calculators/moneyline-converter
AN_ML_CONV = {
        0   : {'fav_ml': -110, 'dog_ml':-110},
        -1  : {'fav_ml': -106, 'dog_ml':-114},
        -1.5: {'fav_ml': -118, 'dog_ml':-102},
        -2:   {'fav_ml': -134, 'dog_ml': 111},
        -2.5: {'fav_ml': -163, 'dog_ml': 134},
        -3:   {'fav_ml': -149, 'dog_ml': 123},
        -3.5: {'fav_ml': -177, 'dog_ml': 145},
        -4:   {'fav_ml': -176, 'dog_ml': 144},
        -4.5: {'fav_ml': -202, 'dog_ml': 164},
        -5:   {'fav_ml': -212, 'dog_ml': 172},
        -5.5: {'fav_ml': -236, 'dog_ml': 190},
        -6:   {'fav_ml': -283, 'dog_ml': 224},
        -6.5: {'fav_ml': -281, 'dog_ml': 223},
        -7:   {'fav_ml': -333, 'dog_ml': 259},
        -7.5: {'fav_ml': -342, 'dog_ml': 265},
        -8:   {'fav_ml': -431, 'dog_ml': 324},
        -8.5: {'fav_ml': -443, 'dog_ml': 331},
        -9:   {'fav_ml': -545, 'dog_ml': 394},
        -9.5: {'fav_ml': -538, 'dog_ml': 389},

        -10:  {'fav_ml': -697, 'dog_ml': 478},
        -10.5:{'fav_ml': -902, 'dog_ml': 578},
        -11:  {'fav_ml': -843, 'dog_ml': 551},
        -11.5:{'fav_ml': -912, 'dog_ml': 583},
        -12:  {'fav_ml': -934, 'dog_ml': 593},
        -12.5:{'fav_ml': -1572, 'dog_ml': 831},
        -13:  {'fav_ml': -1115, 'dog_ml': 670},
        -13.5:{'fav_ml': -2539, 'dog_ml': 1070},
        -14:  {'fav_ml': -2088, 'dog_ml': 972},
        -14.5:{'fav_ml': -12400, 'dog_ml': 1699},
        -15:  {'fav_ml': -3563, 'dog_ml': 1235},
        -15.5:{'fav_ml': -4267, 'dog_ml': 1318},
    }


def get_actionnetwork_moneyline_conversion():
    return AN_ML_CONV