import requests
from bs4 import BeautifulSoup


def get_boydbets_moneyline_conversion():
    url = 'https://www.boydsbets.com/nba-spread-to-moneyline-conversion/'
    response = requests.get(url=url, headers=
    {
        'accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'referer':'https://www.boydsbets.com/money-line-conversion-chart/',
        'user-agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_16_1) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/87.0.4280.140 Safari/537.35',
    })
    bs = BeautifulSoup(response.content, 'html.parser')
    tables = bs.select('table')
    moneylines_dict = {}
    for table in tables:
        for tr in table.select('tbody > tr'):
            tds = tr.select('td')
            line = float(tds[0].string)
            fav_ml = float(tds[6].string)
            dog_ml = float(tds[7].string)
            moneylines_dict[line] = {
                'fav_ml': fav_ml,
                'dog_ml': dog_ml,
            }

    return moneylines_dict


result = get_boydbets_moneyline_conversion()
print('done')