import requests

from src.cache import get_cache

DEFAULT_CACHE_TIMEOUT = 24 * 3600 * 365

headers = {'x-api-key':'57a62c2a-4aa6-11eb-89ba-0ae9bc51dafd'}
cache = get_cache()


@cache.cached(timeout=DEFAULT_CACHE_TIMEOUT, key_prefix='jsonodds/get_test_odds')
def get_test_odds():
    return requests.get(url='https://jsonodds.com/api/test/odds').json()


@cache.cached(timeout=DEFAULT_CACHE_TIMEOUT, key_prefix='jsonodds/get_test_results')
def get_test_results():
    return requests.get(url='https://jsonodds.com/api/test/results').json()


@cache.memoize(timeout=DEFAULT_CACHE_TIMEOUT)
def get_sports():
    return requests.get(url='https://jsonodds.com/api/sports', headers=headers).json()


@cache.memoize(timeout=DEFAULT_CACHE_TIMEOUT)
def get_odds(sport_name):
    return requests.get(url=f'https://jsonodds.com/api/odds/{sport_name}/', headers=headers).json()

# odds = get_test_odds()
# results = get_test_results()
sports = get_sports()
inv_map = {v: k for k, v in sports.items()}
nba_id = inv_map['nba']
nba_odds = get_odds('nba')

print('done')