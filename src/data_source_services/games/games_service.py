import datetime
import json
import os
from calendar import monthrange

import pandas as pd
from basketball_reference_web_scraper.data import TEAM_TO_TEAM_ABBREVIATION, Outcome, Team
from pandas import DataFrame
from typing import List

from cache import get_file_system_cache, DEFAULT_CACHE_TIMEOUT, CACHE_DIR
from featurizer.nba_season_service import get_season_date_range, get_last_date_of_regular_season
from data_source_services.games.basketballreference_com import get_bbref_team_box_score



def myconverter(o):
    if isinstance(o, datetime.datetime) or isinstance(o, datetime.date):
        return o.__str__()

import re
import dateutil
pattern = "\d{4}[/-]\d{1,2}[/-]\d{1,2}"


def mydeserializer(o: object):
    if isinstance(o, dict):
        if 'outcome' in o:
            o['outcome'] = Outcome[o['outcome'].split('.')[-1]]
        if 'team' in o:
            o['team'] = Team[o['team'].split('.')[-1]]
    return o


def get_games_for_date(dateobj: datetime.date) -> List:
    dd = dateobj.strftime("%Y-%m-%d")
    file = f'{CACHE_DIR}games-{dd}.json'
    candidate_date =  datetime.datetime.now().date() - datetime.timedelta(days=3)
    print(f'file=\t{file}')
    if (dateobj < candidate_date) and os.path.exists(file):
        with open(file, 'r') as myfile:
            try:
                contents = myfile.read()
                return json.loads(contents, object_hook=mydeserializer)
            except Exception as e:
                return []

    else:
        result = get_games_for_date_uncached(dateobj)
        with open(file, 'w') as myfile:
            try:
                myfile.write(json.dumps(result, default=myconverter))
            except TypeError as te:
                myfile.write('[]')
        return result


def get_games_for_date_uncached(dateobj: datetime.date) -> List:
    count = 0
    g = []
    if dateobj.day == 0:
        return g
    box = get_bbref_team_box_score(year=dateobj.year, month=dateobj.month, day=dateobj.day)
    while count < len(box):
        obj = {
            'visitor': box[count],
            'home': box[count + 1],
            'date': dateobj,
        }
        g.append(obj)
        count += 2
        for modder in [obj['visitor'], obj['home']]:
            modder['abbrev'] = TEAM_TO_TEAM_ABBREVIATION[modder['team']]
            # team = f"{modder['team']}"
            modder['team'] = f"{modder['team']}"
            modder['outcome'] = f"{modder['outcome']}"
        if obj['home']['outcome'] == f'{Outcome.WIN}':
            obj['winner'] = obj['home']['abbrev']
        else:
            obj['winner'] = obj['visitor']['abbrev']
        obj['game_id'] = '{}-{}-{}'.format(dateobj.isoformat(), obj['home']['abbrev'], obj['visitor']['abbrev'])
    return g


# Do not @cache here, cache per-game
def get_games(date_range) -> List:
    games_arr = []
    long_cache_date = (datetime.datetime.now() - datetime.timedelta(days=3)).date()
    for dr in date_range:
        for month in dr['months']:
            year = dr['year']
            if 'days' in dr:
                ranger = dr['days']
            else:
                ranger = monthrange(year, month)
            for day in range( 1, ranger[1] + 1):
                print(f'date={year}-{month}-{day}')
                d = datetime.date(year=year, month=month, day=day)

                if d < long_cache_date:
                    g = get_games_for_date(d)
                else:
                    g = get_games_for_date_uncached(d)
                games_arr = games_arr + g
    return games_arr


# @functools.lru_cache(maxsize=100, typed=False)
def get_games_featurized_df(date_range=None, season=None, playoffs=False) -> DataFrame:
    """
    :param date_range:
    :param season: numeric year-ending season
    :return: a DataFrame with features:
        Index(['date', 'winner', 'visitor.team', 'visitor.outcome',
               'visitor.minutes_played', 'visitor.made_field_goals',
               'visitor.attempted_field_goals', 'visitor.made_three_point_field_goals',
               'visitor.attempted_three_point_field_goals', 'visitor.made_free_throws',
               'visitor.attempted_free_throws', 'visitor.offensive_rebounds',
               'visitor.defensive_rebounds', 'visitor.assists', 'visitor.steals',
               'visitor.blocks', 'visitor.turnovers', 'visitor.personal_fouls',
               'visitor.points', 'visitor.abbrev', 'home.team', 'home.outcome',
               'home.minutes_played', 'home.made_field_goals',
               'home.attempted_field_goals', 'home.made_three_point_field_goals',
               'home.attempted_three_point_field_goals', 'home.made_free_throws',
               'home.attempted_free_throws', 'home.offensive_rebounds',
               'home.defensive_rebounds', 'home.assists', 'home.steals', 'home.blocks',
               'home.turnovers', 'home.personal_fouls', 'home.points', 'home.abbrev'],
              dtype='object')
    """
    print('get_games_featurized_df()')
    if date_range is None and season is None:
        raise Exception("date_range XOR season required")
    if date_range is None and season is not None:
        date_range = get_season_date_range(season)
    elif date_range is not None and season is None:
        season = date_range[-1]['year']
    games = get_games(date_range=date_range)
    df = pd.json_normalize(games)
    df.set_index(['game_id'], inplace=True)
    df['date'] = pd.to_datetime(df['date'])
    df['date'] = df['date'].astype('datetime64')
    if playoffs:
        raise Exception("todo: add rest of season for playoffs")
    else:
        final_regular_season_game_date = get_last_date_of_regular_season(season)
        mask = df['date'].apply(lambda d: d <= final_regular_season_game_date)
        df = df[mask]
    return df
