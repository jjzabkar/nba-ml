from calendar import monthrange

from data_source_services.games.basketballreference_com import get_bbref_team_box_score


def fetch_box_scores(date_range):
    box_scores = []
    for dr in date_range:
        for month in dr['months']:
            year = dr['year']
            ranger = monthrange(year, month)
            for day in range(ranger[0], ranger[1] + 1):
                print(f'date={year}-{month}-{day}')
                box = get_bbref_team_box_score(year=year, month=month, day=day)
                print(f'box={box}')
                box_scores = box_scores + box
    return box_scores
