# https://jaebradley.github.io/basketball_reference_web_scraper/api/
from basketball_reference_web_scraper import client

from src.cache import get_cache

DEFAULT_CACHE_TIMEOUT = 24 * 3600 * 365

cache = get_cache()


@cache.memoize(timeout=DEFAULT_CACHE_TIMEOUT)
def get_bbref_player_box_score(year, month, day):
    print('get_player_box_score')
    return client.player_box_scores(year=year, month=month, day=day)


def get_bbref_team_box_score_uncached(year, month, day):
    print(f'invoked get_bbref_team_box_score {year}-{month}-{day}')
    return client.team_box_scores(year=year, month=month, day=day)


# @cache.memoize(timeout=DEFAULT_CACHE_TIMEOUT) ## UnpicklingErrors
def get_bbref_team_box_score(year, month, day):
    return get_bbref_team_box_score_uncached(year, month, day)


if __name__ == '__main__':
    player_box_score = get_bbref_player_box_score(2020, 12, 30)
    player_box_score = get_bbref_player_box_score(2020, 12, 30)
    team_box_score = get_bbref_team_box_score(2020, 12, 30)
    team_box_score = get_bbref_team_box_score(2020, 12, 30)

    print('done')
