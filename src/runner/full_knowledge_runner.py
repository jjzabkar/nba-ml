from autogluon_models import ag_games_v5

###
### a model train/test runner that uses full knowledge of known games,
### as opposed to time-series based knowledge.
###
from featurizer.nba_season_service import SEASON_DATE_RANGES

NUM_TRIALS = [None, 10, 50, 100]
TIME_LIMITS = [60, 300, 600]
RANDOM_STATES = [44, 101, 102, 103, 104]

## For testing:
# SEASON_DATE_RANGES = [SEASON_2019_DATE_RANGE]
NUM_TRIALS = [None, 100]
TIME_LIMITS = [300]
# RANDOM_STATES = [44]
# OBJECTIVES=['winner', 'home.against_the_spread_result']

for num_trials in NUM_TRIALS:
    for time_limit in TIME_LIMITS:
        for random_state in RANDOM_STATES:
            for season_date_range in SEASON_DATE_RANGES:
                print(f'***\nRun model ag_games_v1 for time_limit={time_limit} and random_state={random_state}\n***',
                      flush=True)
                szn = season_date_range[-1]['year']
                note = f"{szn}. trials={num_trials} time={time_limit}"
                # ag_games_v1.train_test_model(
                #     date_range=season_date_range,
                #     random_state=random_state,
                #     time_limit=time_limit,
                #     note=note,
                #     num_trials=num_trials,
                # )
                #
                # ag_games_v2.train_test_model(
                #     date_range=season_date_range,
                #     random_state=random_state,
                #     time_limit=time_limit,
                #     note=note,
                #     num_trials=num_trials,
                # )
                #
                # ag_games_v3.train_test_model(
                #     date_range=season_date_range,
                #     random_state=random_state,
                #     time_limit=time_limit,
                #     note=note,
                #     num_trials=num_trials,
                #     objective='winner',   ##   'home.against_the_spread_result' no bueno
                # )
                #
                # ag_games_v4.train_test_model(
                #     date_range=season_date_range,
                #     random_state=random_state,
                #     time_limit=time_limit,
                #     note=note,
                #     num_trials=num_trials,
                #     objective='winner',
                # )

                ag_games_v5.train_test_model(
                    date_range=season_date_range,
                    random_state=random_state,
                    time_limit=time_limit,
                    note=note,
                    num_trials=num_trials,
                    objective='winner',
                )