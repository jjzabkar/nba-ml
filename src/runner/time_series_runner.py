from autogluon_models import ag_games_v6_ts, ag_games_v7_ts
from featurizer.nba_season_service import SEASON_DATE_RANGES

num_bagging_folds = 10  # [None, 5, 7, 10] # 5-10
stack_ensemble_levels_arr = [3]  # [None, 1, 2, 3] # 1-3
time_limit_arr = [None]  # seconds. Use 'None' per AG team.
timeframe_days = [3]  # , 5, 7]
eval_metrics = ['f1']  # ['accuracy', 'f1']
presets = 'best_quality_with_high_quality_refit'
cutoffs = [0.6, 0.67, 0.75]
SAMPLE_COUNT = 10

test_count = 0
for cutoff in cutoffs:
    for season_date_range in SEASON_DATE_RANGES:
        for stack_ensemble_levels in stack_ensemble_levels_arr:
            for eval_metric in eval_metrics:
                for timeframe_day in timeframe_days:
                    for time_limit in time_limit_arr:
                        for sample in range(0, SAMPLE_COUNT):
                            test_count += 1

print(f'test_count={test_count}')
# est_runtime = test_count * (mean(time_limit_arr) + 30)
# print(f'estimated runtime: = {est_runtime}s, \t {round((est_runtime/60),1)} min, \t {round((est_runtime/3600),1)} hrs ')

for cutoff in cutoffs:
    for season_date_range in SEASON_DATE_RANGES:
        for stack_ensemble_levels in stack_ensemble_levels_arr:
            for eval_metric in eval_metrics:
                for timeframe_day in timeframe_days:
                    for time_limit in time_limit_arr:
                        for sample in range(0, SAMPLE_COUNT):
                            try:
                                ag_games_v6_ts.train_test_model(
                                    date_range=season_date_range,
                                    ensuing_timeframe_to_test_days=timeframe_day,
                                    eval_metric=eval_metric,
                                    num_bagging_folds=num_bagging_folds,
                                    save_space=True,
                                    stack_ensemble_levels=stack_ensemble_levels,
                                    time_limit=time_limit,
                                    training_cutoff_pct=cutoff,
                                )
                            except Exception as ex:
                                print(
                                    f'run failed: cutoff={cutoff}, season_date_range={season_date_range}, eval_metric={eval_metric}, timeframe_day={timeframe_day}, sample={sample}: {ex}')

                            try:
                                ag_games_v7_ts.train_test_model(
                                    date_range=season_date_range,
                                    ensuing_timeframe_to_test_days=timeframe_day,
                                    eval_metric=eval_metric,
                                    num_bagging_folds=num_bagging_folds,
                                    save_space=True,
                                    stack_ensemble_levels=stack_ensemble_levels,
                                    time_limit=time_limit,
                                    training_cutoff_pct=cutoff,
                                )
                            except Exception as ex:
                                print(
                                    f'run failed: cutoff={cutoff}, season_date_range={season_date_range}, eval_metric={eval_metric}, timeframe_day={timeframe_day}, sample={sample}: {ex}')
