from pandas import DataFrame

from src.reporting.google_client import append_model_training_results, append_roi_over_time, append_list_to_spreadsheet


class ModelTrainingResult:
    model: str = ''
    objective: str = ''
    random_seed: int = -1
    accuracy: float = 0.0
    f1_score: float = 0.0
    training_seconds: float = 0.0
    notes: str = ''
    feature_list: list = []
    dataset_shape: str = ''
    point_spread_bet_payouts: float = 0.0
    money_line_bet_payouts: float = 0.0
    roi: float = 0.0
    bet_count: int = 0
    test_shape: tuple

    def __init__(self):
        'noop'

    def write(self):
        arr = [
            self.model,
            self.objective,
            len(self.feature_list) - 1,
            self.random_seed,
            self.accuracy,
            self.f1_score,
            self.training_seconds,
            self.notes,
            f'{self.feature_list}',
            f'{self.dataset_shape}',
            f'{self.point_spread_bet_payouts}',
            f'{self.money_line_bet_payouts}',
            f'{self.roi}',
            self.bet_at_risk_amount,
            self.bet_count,
        ]
        append_model_training_results(arr)

    def plot_roi_over_time(self):
        arr = [
            f'{self.dataset_shape[0]}',
            f'{self.accuracy}',
            f'{self.roi}',
            self.model,
            self.notes,
        ]
        append_roi_over_time(arr)

    def log_decile_prediction_distribution(self, df: DataFrame, decile_colname='decile', correct_colname='is_correct', money_line_colname='money_line_bet_payout', bet_amount=100.0):
        prediction_accuracy = []
        money_line_bet_payouts = []
        for i in range(0, 10):
            mask = df[decile_colname] >= i
            df2 = df[mask]
            sum = df2[correct_colname].sum()
            total = df2.shape[0]
            prediction_accuracy.append(sum/total)
            money_line_bet_payout = df2[money_line_colname].sum()
            money_line_bet_payouts.append((money_line_bet_payout/(total * 100.0))-1.0)

        arr = [
            self.model,
            self.notes,
        ]
        arr = arr + prediction_accuracy
        arr = arr + money_line_bet_payouts
        append_list_to_spreadsheet(arr=arr, spreadsheet_tab='decile-predictions')


if __name__ == '__main__':
    model_training_result = ModelTrainingResult()
    model_training_result.model = 'testmodel'
    model_training_result.objective = 'objective_column_name'
    model_training_result.random_seed = 44
    model_training_result.accuracy = 0.3
    model_training_result.f1_score = 0.8
    model_training_result.training_seconds = 1.23
    model_training_result.notes = 'created by ModelTrainingResult.py:main'
    model_training_result.feature_list = ['foo', 'bar']
    model_training_result.write()