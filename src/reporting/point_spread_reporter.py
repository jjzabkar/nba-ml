import math


def round_half_up(n, decimals=0):
    multiplier = 10 ** decimals
    return math.floor(n*multiplier + 0.5) / multiplier


def get_bet_payout(
        bet_amount,  # USD
        bet_payout,  # W/L/P
        odds,
        payout=False,
):
    """
    see: https://www.oddsshark.com/sports-betting/betting-money-line
    :param bet_amount: bet amount, in USD
    :param bet_payout: W[in], L[oss], P[ush]
    :param odds: the american moneyline odds
    :return:
    """
    assert bet_payout in ['W', 'L', 'P'], "expected W|L|P"
    assert abs(odds) >= 100, "expected American odds > abs(100)"
    if bet_payout == 'P':
        return bet_amount
    elif bet_payout == 'L':
        return 0 # -bet_amount
    else:
        result = 0

        if odds > 0:  # underdog
            result = round_half_up(bet_amount * odds / 100.0, 2)
        else:  # favorite
            result = round_half_up(bet_amount * 100.0 / (-odds), 2)
        if payout:
            result = result + bet_amount
        return result
