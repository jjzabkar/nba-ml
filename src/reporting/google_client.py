from __future__ import print_function

import datetime
import os.path
import pickle

from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets',
          'https://www.googleapis.com/auth/drive',
          'https://www.googleapis.com/auth/drive.file']

SAMPLE_SPREADSHEET_ID = '1nO6YIcteXqrc6Q3_IdGufaETgQ_h9nqwpbatUZqiuXc'
# The ID and range of a sample spreadsheet.
SAMPLE_RANGE_NAME = 'model-training-results!A1:E'
PICKLE_TOKEN_FILE = '/Users/jjzabkar/.config/gspread/token.pickle'


def get_spreadsheet(spreadsheet_id=SAMPLE_SPREADSHEET_ID, spreadsheet_tab='model-training-results',
                    spreadsheet_range='A1:E1'):
    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists(PICKLE_TOKEN_FILE):
        with open(PICKLE_TOKEN_FILE, 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                '/Users/jjzabkar/.config/gspread/credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open(PICKLE_TOKEN_FILE, 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)

    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=spreadsheet_id,
                                range=f'{spreadsheet_tab}!{spreadsheet_range}').execute()
    values = result.get('values', [])

    if not values:
        print('No data found.')
    else:
        print('Data:')
        for row in values:
            # Print columns A and E, which correspond to indices 0 and 4.
            print('%s, %s' % (row[0], row[4]))
    return sheet


def append_list_to_spreadsheet(arr: list, spreadsheet_tab: str):
    try:
        spreadsheet = get_spreadsheet()
        body = {'values': [[f'{datetime.datetime.now()}'] + arr]}
        spreadsheet_range = "A1"
        range_name = f'{spreadsheet_tab}!{spreadsheet_range}'
        result = spreadsheet.values().append(
            spreadsheetId=SAMPLE_SPREADSHEET_ID,
            range=range_name,
            insertDataOption='INSERT_ROWS',
            valueInputOption="USER_ENTERED",
            body=body).execute()
        return result
    except Exception as e:
        print(f'unable to log to spreadsheet tab {spreadsheet_tab}: {e}')


def append_model_training_results(arr: list):
    return append_list_to_spreadsheet(arr=arr, spreadsheet_tab='model-training-results')


def append_roi_over_time(arr: list):
    return append_list_to_spreadsheet(arr=arr, spreadsheet_tab='roi-over-time')
