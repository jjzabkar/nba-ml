from pandas import DataFrame

from data_source_services.odds.actionnetwork import get_actionnetwork_moneyline_conversion
from featurizer.featureizer_helper import initialize_and_validate_df
from src.reporting.point_spread_reporter import get_bet_payout


def _lambda_lookup_moneyline_columns(home_point_spread, conversion_map, bet_amount):
    """
    Use point spread to derive the money line.
    Calculator: https://www.actionnetwork.com/betting-calculators/moneyline-converter
    NB: Tuple faster than Series: https://stackoverflow.com/a/48134659/237225
    :param home_point_spread == home.point_spread
    :param conversion_map
    :return a 4-tuple with:
        home.money_line
        home.money_line_bet_payout
        visitor.money_line
        visitor.money_line_bet_payout
        money_line_bet_payout
    """
    home_fav = True
    hps = home_point_spread
    if home_point_spread > 0:
        home_fav = False
        hps = -home_point_spread
    try:
        fav_ml = conversion_map[hps]['fav_ml']
    except Exception:
        fav_ml = conversion_map[-15.5]['fav_ml']
    try:
        dog_ml = conversion_map[hps]['dog_ml']
    except Exception:
        dog_ml = conversion_map[-15.5]['dog_ml']
    fav_bet_payout = get_bet_payout(bet_amount=bet_amount, bet_payout='W', odds=fav_ml, payout=True)
    dog_bet_payout = get_bet_payout(bet_amount=bet_amount, bet_payout='W', odds=dog_ml, payout=True)

    if home_fav:
        return fav_ml, fav_bet_payout, dog_ml, dog_bet_payout
    else:
        return dog_ml, dog_bet_payout, fav_ml, fav_bet_payout


def get_money_line_feature_df(
        df: DataFrame,
        bet_amount=100.0,
) -> DataFrame:
    """
    :param df:
    :param bet_amount:
    :return: Features: 'home.money_line', 'home.money_line_bet_payout', 'visitor.money_line', 'visitor.money_line_bet_payout'
    """
    print(f'get_money_line_feature_df( {df.shape} )')
    output_df = initialize_and_validate_df(df=df, expected_columns=['home.point_spread', 'winner', 'home.abbrev', 'visitor.abbrev'])

    ml_conv = get_actionnetwork_moneyline_conversion()

    output_df['home.money_line'], \
    output_df['home.money_line_bet_payout'], \
    output_df['visitor.money_line'], \
    output_df['visitor.money_line_bet_payout'] = \
        zip(*df['home.point_spread'].apply(
            lambda row: _lambda_lookup_moneyline_columns(row, ml_conv, bet_amount=bet_amount)))
    return output_df #[
        #['home.money_line', 'home.money_line_bet_payout', 'visitor.money_line', 'visitor.money_line_bet_payout']]


def _lambda_calculate_predicted_money_line_bet_payout(row, bet_amount):
    if row['y_pred'] != row['winner']:
        return 0 # -bet_amount
    elif row['winner'] == row['home.abbrev']:
        return row['home.money_line_bet_payout']
    elif row['winner'] == row['visitor.abbrev']:
        return row['visitor.money_line_bet_payout']
    else:
        raise Exception(f"NO WORKY: {row}")


def calculate_money_line_bet_payouts_df(bet_size, y_pred, x_test, objective) -> DataFrame:
    return calculate_money_line_bet_payouts_df2(scaling_factor=bet_size, y_pred=y_pred, x_test=x_test, objective='winner')


def _lambda_calculate_predicted_money_line_bet_payout2(row, scaling_factor, objective_col, prediction_col):
    if row[prediction_col] != row[objective_col]:
        return 0
    elif row['winner'] == row['home.abbrev']:
        return row['home.money_line_bet_payout'] * scaling_factor
    elif row['winner'] == row['visitor.abbrev']:
        return row['visitor.money_line_bet_payout'] * scaling_factor
    else:
        raise Exception(f"NO WORKY: {row}")


def calculate_money_line_bet_payouts_df2(bet_size: float, y_pred: DataFrame, x_test: DataFrame, objective: str) -> DataFrame:
    print('calculate_money_line_bet_payouts_df()')
    scaling_factor = (bet_size / 100.0)
    required_features = ['home.point_spread', 'home.abbrev', 'visitor.abbrev', 'winner', objective]
    assert x_test.index.name == 'game_id', 'expected game_id index'
    for required_feature in required_features:
        assert x_test[required_feature] is not None, f'feature required: {required_feature}'

    df = DataFrame(x_test[required_features])
    assert df.index.name == 'game_id', 'expected game_id index 2'

    df['y_pred'] = y_pred
    ml_df = get_money_line_feature_df(df, bet_amount=bet_size)

    df[ml_df.columns] = ml_df

    df['money_line_bet_payout'] = df.apply(
        lambda row: _lambda_calculate_predicted_money_line_bet_payout2(row, scaling_factor=scaling_factor, objective_col=objective, prediction_col='y_pred'), axis=1)

    return df
