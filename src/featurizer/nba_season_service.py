from datetime import date
from typing import Dict, List

from pandas import DataFrame


def get_season_date_range(season_year_end: int) -> List[Dict]:
    if season_year_end == 2020:
        return [
            {'year': (season_year_end - 1), 'months': [10, 11, 12]},
            {'year': season_year_end, 'months': [1, 2, 3, 8]},
        ]
    else:
        return [
            {'year': (season_year_end - 1), 'months': [10, 11, 12]},
            {'year': season_year_end, 'months': [1, 2, 3, 4, 5]},
        ]


def get_last_date_of_regular_season(season_year_end: int) -> date:
    if season_year_end == 2020:
        return date(2020, 8, 15)
    elif season_year_end == 2019:
        return date(2019, 4, 10)
    elif season_year_end == 2018:
        return date(2018, 4, 11)
    elif season_year_end == 2017:
        return date(2017, 4, 12)
    elif season_year_end == 2016:
        return date(2016, 4, 13)
    else:
        raise Exception(f"TODO: configure end of {season_year_end} season date")


SEASON_2016_DATE_RANGE = get_season_date_range(2016)
SEASON_2017_DATE_RANGE = get_season_date_range(2017)
SEASON_2018_DATE_RANGE = get_season_date_range(2018)
SEASON_2019_DATE_RANGE = get_season_date_range(2019)
SEASON_2020_DATE_RANGE = get_season_date_range(2020)

SEASON_DATE_RANGES = [
    # get_season_date_range(2016),
    get_season_date_range(2017),
    get_season_date_range(2018),
    get_season_date_range(2019),
    # get_season_date_range(2020),
]
