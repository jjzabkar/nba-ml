from pandas import DataFrame

from featurizer.featureizer_helper import initialize_and_validate_df

HOME_TEAM_IS_WINNER_COLUMN_NAME = 'home_team_is_winner'


def get_home_team_is_winner_feature(df: DataFrame) -> DataFrame:
    print(f'get_home_team_is_winner_feature( {df.shape} )')
    output_df = initialize_and_validate_df(df=df, expected_columns=['home.abbrev', 'winner'])

    output_df[HOME_TEAM_IS_WINNER_COLUMN_NAME] = df.apply(lambda row: _lambda_get_home_team_is_winner_feature(row), axis=1)
    output_df[HOME_TEAM_IS_WINNER_COLUMN_NAME] = output_df[HOME_TEAM_IS_WINNER_COLUMN_NAME].astype('bool')
    return output_df


def _lambda_get_home_team_is_winner_feature(row) -> bool:
    return row['home.abbrev'] == row['winner']
