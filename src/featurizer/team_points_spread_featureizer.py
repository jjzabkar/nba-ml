import os
import pandas as pd

from pandas import DataFrame, Series

from data_source_services.odds.odds_shark import get_ats_values_for_season, lambda_lookup_points_spread_column
from featurizer.featureizer_helper import initialize_and_validate_df
from src.reporting.point_spread_reporter import get_bet_payout
from src.cache import CACHE_DIR

TEAM_POINTS_SPREAD_FEATURE_COLUMN_TO_DTYPES = {
    'home.point_spread': 'float64',
    'home.point_spread_bet_payout': 'float64',
    'visitor.point_spread': 'float64',
    'visitor.point_spread_bet_payout': 'float64',
}


def lambda_get_point_spread_bet_payout(row, ats_values, abbrev, bet_amount, spread_odds=-110):
    ats_result = lambda_lookup_points_spread_column(row, ats_values, abbrev, 'against_the_spread_result')
    bet_payout = get_bet_payout(bet_amount, ats_result, spread_odds)
    return bet_payout


# @functools.lru_cache(maxsize=100, typed=False)
def get_points_spread_feature_df(
        df: DataFrame,
        season: int,
        ats_values_df=None,
        do_ats_result=True,
        do_over_under=True,
        do_evaluate_money_line_result_bet_amount=100.0,
) -> DataFrame:
    """

    :param df:
    :param season: the end year of the season  (e.g. "2020" for 2019-2020)
    :return: a DataFrame with a features:
        home.point_spread
        home.against_the_spread_result
        visitor.point_spread
        visitor.against_the_spread_result
        over_under
        over_under_total
    """
    print(f'get_points_spread_feature_df( {df.shape} )')

    cache_file_name = f'{CACHE_DIR}{season}-points_spread_feature_df-({df.shape[0]}-{df.shape[1]})-{ats_values_df}-{do_ats_result}-{do_over_under}-{do_evaluate_money_line_result_bet_amount}.csv'
    if os.path.exists(cache_file_name):
        result = pd.read_csv(cache_file_name)
        result.set_index(['game_id'], inplace=True)
        for k, v in TEAM_POINTS_SPREAD_FEATURE_COLUMN_TO_DTYPES.items():
            result[k] = result[k].astype(v)
        return result

    if ats_values_df is None:
        assert season is not None and season > 0, "season required"
        ats_values_df = get_ats_values_for_season(season)

    output_df = initialize_and_validate_df(df=df, expected_columns=['home.abbrev', 'visitor.abbrev'])

    for tm in ['home', 'visitor']:
        print(f'featurizing {tm}.point_spread', flush=True)
        output_df[f'{tm}.point_spread'] = df.apply(
            lambda row: lambda_lookup_points_spread_column(row, ats_values_df, abbrev=f'{tm}.abbrev',
                                                           column_name='point_spread'), axis=1)

        print(f'featurizing {tm}.against_the_spread_result', flush=True)
        output_df[f'{tm}.against_the_spread_result'] = df.apply(
            lambda row: lambda_lookup_points_spread_column(row, ats_values_df, abbrev=f'{tm}.abbrev',
                                                           column_name='against_the_spread_result'), axis=1)

        if do_evaluate_money_line_result_bet_amount is not None:
            bet_amount = do_evaluate_money_line_result_bet_amount
            print(f'featurizing {tm}.point_spread_bet_payout', flush=True)
            output_df[f'{tm}.point_spread_bet_payout'] = df.apply(
                lambda row: lambda_get_point_spread_bet_payout(row, ats_values_df, abbrev=f'{tm}.abbrev',
                                                               bet_amount=bet_amount), axis=1)

    if do_over_under:
        print(f'featurizing over_under', flush=True)
        output_df['over_under'] = df.apply(
            lambda row: lambda_lookup_points_spread_column(row, ats_values_df, abbrev=f'{tm}.abbrev',
                                                           column_name='over_under'), axis=1)
        print(f'featurizing over_under_total', flush=True)
        output_df['over_under_total'] = df.apply(
            lambda row: lambda_lookup_points_spread_column(row, ats_values_df, abbrev=f'{tm}.abbrev',
                                                           column_name='total'), axis=1)

    output_df.to_csv(cache_file_name)
    return output_df


def all_equal(a, b, c):
    return a == b and b == c


def lambda_calculate_point_spread_bet_payouts(row, bet_size, scaling_factor):
    return lambda_calculate_point_spread_bet_payouts2(row, bet_size, scaling_factor, 'winner')


def lambda_calculate_point_spread_bet_payouts2(row: Series, bet_size: float, scaling_factor: float, objective: str) -> float:
    if row['y_pred'] != row[objective]:
        return 0
    elif 'W' == row['home.against_the_spread_result']:
        return row['home.point_spread_bet_payout'] * scaling_factor
    elif 'L' == row['home.against_the_spread_result']:
        return row['visitor.point_spread_bet_payout'] * scaling_factor
    elif 'P' == row['home.against_the_spread_result']:
        return bet_size
    else:  # lif row['home.against_the_spread_result'] != row['y_pred']:
        return 0


def calculate_point_spread_bet_payouts_df(bet_size: float, y_pred: DataFrame, x_test: DataFrame) -> DataFrame:
    return calculate_point_spread_bet_payouts_df2(bet_size, y_pred, x_test, 'winner')


def calculate_point_spread_bet_payouts_df2(bet_size: float, y_pred: DataFrame, x_test: DataFrame, objective: str) -> DataFrame:
    print('calculate_point_spread_bet_payouts()')
    scaling_factor = (bet_size / 100.0)
    required_features = ['home.against_the_spread_result', 'home.point_spread_bet_payout',
                         'visitor.point_spread_bet_payout', 'home.point_spread', 'home.abbrev',
                         'visitor.abbrev', 'winner', objective]
    assert x_test.index.name == 'game_id', 'expected game_id index'
    for required_feature in required_features:
        assert x_test[required_feature] is not None, f'feature required: {required_feature}'
    df = DataFrame(
        x_test[['home.against_the_spread_result', 'home.point_spread_bet_payout',
                'visitor.point_spread_bet_payout', objective]])
    df['y_pred'] = y_pred
    df['point_spread_bet_payout'] = df.apply(
        lambda row: lambda_calculate_point_spread_bet_payouts2(row, bet_size, scaling_factor, objective), axis=1)
    return df['point_spread_bet_payout']
