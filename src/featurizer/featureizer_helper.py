from pandas import DataFrame
from typing import List


def initialize_and_validate_df(df: DataFrame, expected_columns: List[str], required_index='game_id',) -> DataFrame:
    """
    validate expected columns and initialize the DataFrame's index.
    :param df:
    :param expected_columns:
    :param required_index:
    :return:
    """
    assert df.index.name == required_index, f'expected index: {required_index}'
    for expected_column in expected_columns:
        assert df[expected_column] is not None, f'expected input column "{expected_column}"'
    nonindex_column = df.columns[0]
    result = DataFrame(data=df[nonindex_column], index=df.index).drop([nonindex_column], axis=1)
    return result
