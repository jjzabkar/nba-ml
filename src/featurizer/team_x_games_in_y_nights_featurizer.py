import datetime

from pandas import DataFrame, Series

from data_source_services.games.games_service import get_games_featurized_df
from featurizer.featureizer_helper import initialize_and_validate_df
from featurizer.team_days_since_last_game_featurizer import get_nba_schedule_cached


def _lambda_get_x_games_in_y_nights(row: Series, schedule: dict, column_count: int, team_abbrev:str):
    team = row[team_abbrev]
    team_schedule = schedule[team]['by_date']
    curr_date = row['date']
    result = []
    for i in range(1,column_count+1):
        value = 'R'
        candidate_date = curr_date - datetime.timedelta(days=i)
        if candidate_date in team_schedule:
            if team_schedule[candidate_date]['is_home']:
                value = 'H' # HOME
            else:
                value = 'V' # VISITOR
        result.append(value)
    return Series(result)


def get_x_games_in_y_nights_df(df: DataFrame, season: int = None, column_count: int = 6) -> DataFrame:
    print('get_x_games_in_y_nights_df()')
    # pre-process: get season schedule
    if season is None:
        season = df[-2:-1]['date'].values[0].year
    schedule = get_nba_schedule_cached(df=df, season=season)

    output_df = initialize_and_validate_df(df=df, expected_columns=['date', 'home.abbrev', 'visitor.abbrev'])

    for hv in ['home', 'visitor']:
        column_names = []
        for i in range(1, column_count + 1):
            column_names.append(f'{hv}.{i}-nights-ago')
        output_df[column_names] = df.apply(lambda row: _lambda_get_x_games_in_y_nights(row, schedule, column_count, f'{hv}.abbrev'), axis=1)
        for col in column_names:
            output_df[col] = output_df[col].astype('category')
    return output_df


if __name__ == '__main__':
    df = get_games_featurized_df(season=2017)
    result = get_x_games_in_y_nights_df(df, season=2017)
