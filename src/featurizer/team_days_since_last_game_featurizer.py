from pandas import DataFrame
from typing import Dict

from featurizer.featureizer_helper import initialize_and_validate_df

SEASON_SCHEDULE = {}


def get_nba_schedule_cached(df: DataFrame, season: int) -> Dict:
    if season in SEASON_SCHEDULE:
        return SEASON_SCHEDULE[season]
    else:
        result_map = {}
        for index, row in df.iterrows():
            for team in ['home', 'visitor']:
                abbrev = row[f'{team}.abbrev']
                if abbrev not in result_map:
                    result_map[abbrev] = {
                        'by_date': {}
                    }
                days_since_last_game = None

                if 'last_game_date' in result_map[abbrev]:
                    last_game_date = result_map[abbrev]['last_game_date']
                    days_since_last_game = abs((row['date'] - last_game_date).days)
                obj = {
                        'date': row['date'],
                        'days_since_last_game': days_since_last_game,
                        'is_home': (team == 'home'),
                    }
                result_map[abbrev][index] = obj # index == game_id
                result_map[abbrev]['last_game_date'] = row['date']
                result_map[abbrev]['by_date'][row['date']] = obj

        SEASON_SCHEDULE[season] = result_map
        return result_map


def _get_days_since_last_game(row, schedule):
    game_id = row.name # name == index == game_id ## https://stackoverflow.com/a/26658301/237225
    home_dslg = schedule[row['home.abbrev']][game_id]['days_since_last_game']
    visitor_dslg = schedule[row['visitor.abbrev']][game_id]['days_since_last_game']
    return home_dslg, visitor_dslg


def get_team_days_since_last_game_df(
        df: DataFrame,
        season: int,
) -> DataFrame:
    """
    :param df: input DataFrame
    :param season: the (ending year) season to scope game date data
    :return:
        a DataFrame containing feature columns:
        game_id (index)
        home.days_since_last_game
        visitor.days_since_last_game

    """
    output_df = initialize_and_validate_df(df=df, expected_columns=['home.abbrev', 'visitor.abbrev', 'date'])

    # pre-process: get season schedule
    schedule = get_nba_schedule_cached(df=df, season=season)

    output_df[f'home.days_since_last_game'], \
    output_df[f'visitor.days_since_last_game'] \
        = zip(*df.apply(lambda row: _get_days_since_last_game(row, schedule), axis=1))

    return output_df
