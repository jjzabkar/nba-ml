import random
from flask import Flask
from flask_caching import Cache

# docker run -p 6379:6379 --name redis -d redis redis-server --appendonly yes
app = Flask(__name__)
# Check Configuring Flask-Cache section for more details
cache = Cache(app, config={'CACHE_TYPE': 'redis'})
cache.init_app(app)

DEFAULT_CACHE_TIMEOUT = 24 * 3600 * 365
CACHE_DIR = '/Users/jjzabkar/Documents/machine-learning/nba-ml-cache/'

filesystemcache = Cache(app, config={
    'CACHE_TYPE': 'filesystem',
    'CACHE_DIR': CACHE_DIR
})
filesystemcache.init_app(app)


def get_cache():
    return cache


def get_file_system_cache():
    return filesystemcache


@filesystemcache.cached(timeout=5000, key_prefix='test_cache')
def test_cache():
    print('called test cache!!!!')
    return 999


@filesystemcache.memoize(timeout=5000)
def big_foo(a, b):
    print('invoked big_foo()')
    return a + b + random.randrange(0, 1000)


# print('invoke test_cache()')
# test_cache()
# print('invoke test_cache()')
# test_cache()
# print('invoke test_cache()')
# test_cache()
#
# print("invoke big_foo(2,2)")
# print("{}".format(big_foo(2, 2)))
# print("invoke big_foo(2,2)")
# print("{}".format(big_foo(2, 2)))
