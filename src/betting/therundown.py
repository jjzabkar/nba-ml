import requests

# https://medium.com/the-era-of-apis/how-to-create-a-sports-betting-application-with-python-using-therundown-api-d1d3b794b3ee

url = "https://therundown-therundown-v1.p.rapidapi.com/sports"

headers = {
    'x-rapidapi-key': "13669cbc30msh7a48723008728c6p17ac8ejsn41931e8db7fb",
    'x-rapidapi-host': "therundown-therundown-v1.p.rapidapi.com"
    }

response = requests.request("GET", url, headers=headers)

print(response.text)

def get_sports():
    response = requests.get(url, headers=headers)
    return response.json()
