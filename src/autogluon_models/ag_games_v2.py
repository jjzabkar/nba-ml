import os

from src.autogluon_models import get_autogluon_predictions
from data_source_services.games.games_service import get_games_featurized_df


### Hypothesis:
## Using only historical team win-loss records, predict the output of the game.
## X Inputs:
##    date, home.abbrev, away.abbrev
## Y outputs:
##    winner
## Results:
##    58% accuracy for games in 2018-19 season


def train_test_model(date_range, random_state, time_limit, test_size=0.3, note='', num_trials=None):
    """
    :param date_range:
    :param random_state:
    :param time_limit: autogluon constraint
    :param test_size: hyperparameter
    :param note: report field
    :param num_trials: hyperparameter
    :return: written ModelTrainingResult
    """
    games_df = get_games_featurized_df(date_range=date_range)
    return get_autogluon_predictions(
        input_df=games_df,
        feature_columns_list=['home.abbrev', 'visitor.abbrev', 'date'],
        objective='winner',
        note=note,
        model_name=os.path.basename(__file__),
        num_trials=num_trials,
        random_state=random_state,
        test_size=test_size,
        time_limit=time_limit
    )
