import hashlib
import json
import os

import numpy as np
import pandas as pd
from pandas import DataFrame
from typing import List, Dict

from autogluon_models import get_autogluon_predictions_ts
from featurizer.nba_season_service import SEASON_2017_DATE_RANGE, SEASON_2016_DATE_RANGE, SEASON_2019_DATE_RANGE, \
    SEASON_DATE_RANGES, SEASON_2020_DATE_RANGE
from featurizer.team_home_team_is_winner_featurizer import get_home_team_is_winner_feature, \
    HOME_TEAM_IS_WINNER_COLUMN_NAME
from featurizer.money_line_featureizer import get_money_line_feature_df
from featurizer.team_days_since_last_game_featurizer import get_team_days_since_last_game_df
from featurizer.team_x_games_in_y_nights_featurizer import get_x_games_in_y_nights_df
from src.cache import CACHE_DIR
from data_source_services.games.games_service import get_games_featurized_df
from src.featurizer.team_points_spread_featureizer import get_points_spread_feature_df

### Hypothesis:
## Applying bet certainty thresholds (e.g. using confidence interval deciles)
## X Inputs: (same as v7)
##          date, home.abbrev, away.abbrev, home.point_spread, visitor.point_spread,
##          home.days_since_last_game, visitor.days_since_last_game,
##          home.{1-6}-nights-ago, visitor.{1-6}-nights-ago
## Non-feature hyperparameters:
##          Confidence interval threshold (percentage), or decile (int)
## Y outputs:
##    winner (money line)
## Results:
##    at 67% season training data, accuracy=TK f1=TK

OBJECTIVE_COLUMN_NAME = HOME_TEAM_IS_WINNER_COLUMN_NAME
INDEX_COLUMN_NAME = 'game_id'
V8_FEATURE_COLUMN_TO_DTYPES = {
    'home.abbrev': 'category',
    'home.point_spread': 'float',
    'home.days_since_last_game': 'float',
    'visitor.abbrev': 'category',
    'visitor.point_spread': 'float',
    'visitor.days_since_last_game': 'float',
    'home.1-nights-ago': 'category',
    'home.2-nights-ago': 'category',
    'home.3-nights-ago': 'category',
    'home.4-nights-ago': 'category',
    'home.5-nights-ago': 'category',
    'home.6-nights-ago': 'category',
    'visitor.1-nights-ago': 'category',
    'visitor.2-nights-ago': 'category',
    'visitor.3-nights-ago': 'category',
    'visitor.4-nights-ago': 'category',
    'visitor.5-nights-ago': 'category',
    'visitor.6-nights-ago': 'category',
}


def set_column_dtypes(df: DataFrame):
    if df.index is None or df.index.name is None or df.index.name != INDEX_COLUMN_NAME:
        df.set_index([INDEX_COLUMN_NAME], inplace=True)
    for k, v in V8_FEATURE_COLUMN_TO_DTYPES.items():
        df[k] = df[k].astype(v)
    df['date'] = pd.to_datetime(df['date']) ## moved to games_service?
    df['date'] = df['date'].astype('datetime64') ## moved to games_service?

    # set column category types
    for col in ['home.outcome', 'visitor.outcome', 'winner']:
        df[col] = df[col].astype('category')


def load_cacheable_df(date_range: List[Dict], bust_season_cache: bool = False) -> DataFrame:
    flattened = json.dumps(date_range)
    hex = hashlib.md5(flattened.encode()).hexdigest()
    file = f'{CACHE_DIR}{os.path.basename(__file__)}-{hex[0:6]}-source-dataframe.csv'
    if os.path.exists(file) and not bust_season_cache:
        print(f'return cached dataframe file: {file} with index [game_id]')
        df = pd.read_csv(file)
        df.set_index(['game_id'])
        return df

    season = date_range[-1]['year']
    df1 = get_games_featurized_df(date_range=date_range)
    df2 = get_points_spread_feature_df(df=df1, season=season, do_over_under=False, do_ats_result=False)
    df3 = df1.merge(df2, left_on='game_id', right_on='game_id')
    df4 = get_money_line_feature_df(df=df3, bet_amount=100.0)
    df5 = df3.merge(df4, left_on='game_id', right_on='game_id')
    df6 = get_team_days_since_last_game_df(df=df1, season=season)
    df7 = df5.merge(df6, left_on='game_id', right_on='game_id')
    df8 = get_home_team_is_winner_feature(df=df1)
    df9 = df7.merge(df8, left_on='game_id', right_on='game_id')
    df10 = get_x_games_in_y_nights_df(df=df1, season=season)
    df11 = df9.merge(df10, left_on='game_id', right_on='game_id')
    df11.to_csv(file, index=False)

    return df11


def _split_timeseries_data(df, training_cutoff_pct, ensuing_timeframe_to_test_days,
                           min_training_rows=100, min_testing_rows=10):
    print('split_timeseries_data()')
    total_rows = df.shape[0]
    cutoff_row_index = int(training_cutoff_pct * total_rows)
    assert cutoff_row_index > min_training_rows, f"expected at least {min_training_rows} training rows"
    remaining_rows = total_rows - cutoff_row_index
    assert remaining_rows > min_testing_rows, f"expected at least {min_testing_rows} testing rows"
    date_of_cutoff_row = df[cutoff_row_index - 1:cutoff_row_index]['date'].values[0]

    train_data = df[0:cutoff_row_index]
    cutoff_date = date_of_cutoff_row + \
                  np.timedelta64(ensuing_timeframe_to_test_days, 'D')
    test_mask = (df['date'] <= cutoff_date) & (df['date'] > date_of_cutoff_row)
    test_data = df[test_mask]
    assert test_data.shape[0] > 0, "expected at least 1 row of test data"
    print(f'\t train shape: {train_data.shape}\n\t test shape: {test_data.shape}')
    return train_data, test_data


def train_test_model(
        date_range,
        bust_season_cache=False,
        decile=4, # see v8 tests
        training_cutoff_pct=0.5,
        ensuing_timeframe_to_test_days=4,
        log_to_google_spreadsheet=True,
        time_limit=60,
        bet_size=100.0,
        objective=OBJECTIVE_COLUMN_NAME,
        save_space=False,
        eval_metric=None,
        num_bagging_folds=10, # 5-10 recommended per AG team
        stack_ensemble_levels=3, # 1-3 recommended per AG team
):
    """

    :param date_range:
    :param training_cutoff_pct:
    :param ensuing_timeframe_to_test_days:
    :return:
    """
    cached_df = load_cacheable_df(date_range=date_range, bust_season_cache=bust_season_cache)
    set_column_dtypes(df=cached_df)
    season = date_range[-1]['year']

    train_data, test_data = _split_timeseries_data(
        df=cached_df,
        training_cutoff_pct=training_cutoff_pct,
        ensuing_timeframe_to_test_days=ensuing_timeframe_to_test_days)

    return get_autogluon_predictions_ts(
        train_df=train_data,
        test_df=test_data,
        decile=decile,
        ensuing_timeframe_to_test_days=ensuing_timeframe_to_test_days,
        eval_metric=eval_metric,
        feature_columns_list=V8_FEATURE_COLUMN_TO_DTYPES.keys(),
        log_to_google_spreadsheet=log_to_google_spreadsheet,
        model_name=os.path.basename(__file__),
        num_bagging_folds=num_bagging_folds,
        bet_size=bet_size,
        objective=objective,
        save_space=save_space,
        season=season,
        time_limit=time_limit,
        training_cutoff_pct=training_cutoff_pct,
        stack_ensemble_levels=stack_ensemble_levels,
    )


if __name__ == '__main__':
    time_limit = 300  # seconds
    sample_sizes = 10
    cutoff_pcts = [0.42, 0.52, 0.62, 0.72, 0.82]
    deciles = [4] # 0-based

    train_test_model(
        bust_season_cache=True,
        date_range=SEASON_2020_DATE_RANGE,
        decile=deciles[0],
        ensuing_timeframe_to_test_days=3,
        eval_metric='f1',
        save_space=False,
        time_limit=60.0,
        training_cutoff_pct=0.67,
    )

    for decile in deciles:
        for sample_size in range(0, sample_sizes):
            for cutoff_pct in cutoff_pcts:
                for date_range in SEASON_DATE_RANGES:
                    try:
                        train_test_model(
                            date_range=date_range,
                            decile=decile,
                            ensuing_timeframe_to_test_days=3,
                            eval_metric='f1',
                            save_space=True,
                            time_limit=time_limit,
                            training_cutoff_pct=cutoff_pct,
                        )
                    except Exception as ex:
                        print(f'{ex}')
