import autogluon.core as ag
from autogluon.tabular import TabularPrediction as task

print('Load training data from a CSV file into an AutoGluon Dataset object. This object is essentially equivalent to a Pandas DataFrame and the same methods can be applied to both.')

train_data = task.Dataset(file_path='https://autogluon.s3.amazonaws.com/datasets/Inc/train.csv')
subsample_size = 500  # subsample subset of data for faster demo, try setting this to much larger values
train_data = train_data.sample(n=subsample_size, random_state=0)
print(train_data.head())


print('Let’s first use these features to predict whether the person’s income exceeds $50,000 or not, which is recorded in the class column of this table.')

label_column = 'class'
print("Summary of class variable: \n", train_data[label_column].describe())

print('Now use AutoGluon to train multiple models:')

dir = 'agModels-predictClass'  # specifies folder where to store trained models
predictor = task.fit(train_data=train_data, label=label_column, output_directory=dir)

print('Next, load separate test data to demonstrate how to make predictions on new examples at inference time:')

test_data = task.Dataset(file_path='https://autogluon.s3.amazonaws.com/datasets/Inc/test.csv')
y_test = test_data[label_column]  # values to predict
test_data_nolab = test_data.drop(labels=[label_column],axis=1)  # delete label column to prove we're not cheating
print(test_data_nolab.head())

print('We use our trained models to make predictions on the new data and then evaluate performance:')

predictor = task.load(dir)  # unnecessary, just demonstrates how to load previously-trained predictor from file

y_pred = predictor.predict(test_data_nolab)
print("Predictions:  ", y_pred)
perf = predictor.evaluate_predictions(y_true=y_test, y_pred=y_pred, auxiliary_metrics=True)