import hashlib
import json
import os
import pandas as pd

from featurizer.money_line_featureizer import get_money_line_feature_df
from src.cache import CACHE_DIR
from src.autogluon_models import get_autogluon_predictions
from data_source_services.games.games_service import get_games_featurized_df
from src.featurizer.team_points_spread_featureizer import get_points_spread_feature_df


### Hypothesis:
## Using only historical team win-loss records joined with
##    spread and moneyline data, predict the output of the game.
## X Inputs:
##    date, home.abbrev, away.abbrev, home.point_spread, visitor.point_spread,
## Y outputs:
##    winner
## Results:
##    65.2% accuracy for games



def load_cacheable_df(date_range):
    flattened = json.dumps(date_range)
    hex = hashlib.md5(flattened.encode()).hexdigest()
    file = f'{CACHE_DIR}{os.path.basename(__file__)}-{hex[0:6]}-source-dataframe.csv'
    if os.path.exists(file):
        print(f'return cached dataframe file: {file} with index [game_id]')
        df = pd.read_csv(file)
        df.set_index(['game_id'])
        return df

    df1 = get_games_featurized_df(date_range=date_range)
    df2 = get_points_spread_feature_df(df=df1, season=date_range[-1]['year'], do_over_under=False, do_ats_result=False)
    df3 = df1.merge(df2, left_on='game_id', right_on='game_id')
    df4 = get_money_line_feature_df(df=df3, bet_amount=100.0)
    df5 = df3.merge(df4, left_on='game_id', right_on='game_id')

    df5.to_csv(file, index=False)

    return df5


def train_test_model(date_range, random_state, time_limit, objective, test_size=0.3, note='', num_trials=None):
    """
    :param date_range:
    :param random_state:
    :param time_limit: autogluon constraint
    :param test_size: hyperparameter
    :param note: report field
    :param num_trials: hyperparameter
    :return: written ModelTrainingResult
    """
    cached_df = load_cacheable_df(date_range=date_range)

    return get_autogluon_predictions(
        input_df=cached_df,
        feature_columns_list=[
            'home.abbrev',
            'visitor.abbrev',
            'home.point_spread',
            'visitor.point_spread'],
        objective=objective,
        note=note,
        model_name=os.path.basename(__file__),
        num_trials=num_trials,
        random_state=random_state,
        test_size=test_size,
        time_limit=time_limit
    )
