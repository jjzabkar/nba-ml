import datetime
import math
import time
import shutil
import pandas as pd

from autogluon.tabular import TabularPrediction as task
from pandas import DataFrame
from sklearn.model_selection import train_test_split

from featurizer.money_line_featureizer import calculate_money_line_bet_payouts_df, calculate_money_line_bet_payouts_df2
def get_autogluon_predictions_ts(
        train_df,
        test_df,
        feature_columns_list,
        model_name,
        objective,
        decile=None,
        ensuing_timeframe_to_test_days=None,
        log_to_google_spreadsheet=True,
        bet_size=100.0,
        time_limit=None,  # per AG team
        training_cutoff_pct=None,
        ### AutoGluon Hyperparameters:
        eval_metric='f1',  # for binary classification, per AG docs
        num_trials=None,
        num_bagging_folds=10,  # 5-10 per AG docs,
        presets='best_quality_with_high_quality_refit',  # for production, use 'best'
        stack_ensemble_levels=3,  # = 1-3 per AG docs
        save_space=True,
        season=None,
):
    for required_feature in feature_columns_list:
        assert train_df[required_feature] is not None, f'train feature required: {required_feature}'
        assert test_df[required_feature] is not None, f'test feature required: {required_feature}'

    start_time = time.time()
    x_train = train_df[feature_columns_list]
    x_train[objective] = train_df[objective]

    x_test = test_df[feature_columns_list]
    y_test = test_df[objective]

    print(f'Load training data from DataFrame x_train ( {x_train.shape} ) into an AutoGluon Dataset.')
    train_data = task.Dataset(df=x_train)
    print(train_data.head())

    print(f"Summary of objective ({objective}) column: \n", train_data[objective].describe())

    print('Now use AutoGluon to train multiple models:')
    dt = datetime.datetime.now()
    trained_models_dir = f'agModels-{model_name}-ts-{objective}-{dt}'  # specifies folder where to store trained models

    OVERRIDE_DIR = None  # 'agModels-ag_games_v6_ts.py-ts-home_team_is_winner-2021-02-13 20:17:36.993006'
    # OVERRIDE_DIR = '/Users/jjzabkar/git/nba-ml/src/autogluon_models/agModels-ag_games_v8_ts.py-ts-home_team_is_winner-2021-02-23 17:36:34.384613'
    if OVERRIDE_DIR is not None:
        trained_models_dir = OVERRIDE_DIR
        print(f'!!!!!!!!!!!!!!!!\n override model dir: {OVERRIDE_DIR}\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        predictor = task.load(OVERRIDE_DIR)
    else:
        predictor = task.fit(
            eval_metric=eval_metric,
            # 'accuracy', # https://auto.gluon.ai/stable/tutorials/tabular_prediction/tabular-quickstart.html#maximizing-predictive-performance
            label=objective,
            num_trials=num_trials,
            output_directory=trained_models_dir,
            presets=presets,
            time_limits=time_limit,
            train_data=train_data,
            num_bagging_folds=num_bagging_folds,
            stack_ensemble_levels=stack_ensemble_levels,
        )

    print('Next, load separate test data to demonstrate how to make predictions on new examples at inference time:')
    test_data = task.Dataset(df=x_test)
    print(f'Test data: \n{test_data.head()}')

    print('Use trained models to make predictions on the new data and then evaluate performance:')
    y_pred = predictor.predict(test_data)
    print(f'y_pred=\n{y_pred}', flush=True)

    y_proba = predictor.predict_proba(test_data)
    print(f'y_proba=\n{y_proba}', flush=True)

    prediction_df = DataFrame(x_test)
    prediction_df['y_true'] = y_test
    prediction_df['objective'] = y_pred
    prediction_df['y_proba'] = y_proba
    y_decile_df = DataFrame(pd.qcut(y_proba, 10, labels=False, duplicates='drop'))
    decile_deduped = math.floor(max(y_decile_df.values * (decile / 10)))
    prediction_df['decile'] = y_decile_df
    decile_drop_mask = y_decile_df < decile_deduped
    prediction_df['is_correct'] = (prediction_df['y_true'] == prediction_df['objective'])
    prediction_df['drop_me'] = (y_decile_df < decile_deduped)

    prediction_df_decile_masked = prediction_df.loc[~(decile_drop_mask.values), :]
    prediction_df_decile_masked.reset_index(inplace=True)
    prediction_df_decile_masked.set_index(['game_id'], inplace=True)
    print("Predictions:  \n", prediction_df.head())

    x_test_decile_masked = test_df.loc[~(decile_drop_mask.values), :]
    x_test_decile_masked.reset_index(inplace=True)
    x_test_decile_masked.set_index(['game_id'], inplace=True)
    y_test_decile_masked = prediction_df_decile_masked['y_true']
    y_pred_decile_masked = prediction_df_decile_masked['objective']
    bet_count = len(y_pred_decile_masked)

    model_performance_decile_masked = predictor.evaluate_predictions(y_true=y_test_decile_masked, y_pred=y_pred_decile_masked, auxiliary_metrics=True)

    # leaderboard = predictor.leaderboard(extra_info=True, silent=True)
    point_spread_bet_payouts_df_decile_masked = calculate_point_spread_bet_payouts_df2(bet_size=bet_size, y_pred=y_pred_decile_masked,
                                                                     x_test=x_test_decile_masked, objective=objective)
    point_spread_bet_payouts_decile_masked = point_spread_bet_payouts_df_decile_masked.sum()

    money_line_bet_payouts_df_decile_masked = calculate_money_line_bet_payouts_df2(bet_size=bet_size, y_pred=y_pred_decile_masked, x_test=x_test_decile_masked, objective=objective)
    money_line_bet_payouts_sum_decile_masked = money_line_bet_payouts_df_decile_masked['money_line_bet_payout'].sum()

    model_training_result = ModelTrainingResult()
    model_training_result.model = f'{model_name}-ts-{objective}-cutoff-{training_cutoff_pct}'
    model_training_result.objective = objective
    model_training_result.random_seed = 'n/a'
    model_training_result.accuracy = model_performance_decile_masked['classification_report']['accuracy']
    model_training_result.f1_score = model_performance_decile_masked['classification_report']['weighted avg']['f1-score']
    model_training_result.training_seconds = time_limit
    model_training_result.notes = f'cutoff%={training_cutoff_pct}, ' \
                                  f'x_train.shape={x_train.shape}, ' \
                                  f'+days={ensuing_timeframe_to_test_days}, ' \
                                  f'bagging_folds={num_bagging_folds}, ' \
                                  f'stack_ensemble_levels={stack_ensemble_levels}, ' \
                                  f'season={season}'
    model_training_result.feature_list = x_train.columns.values
    model_training_result.dataset_shape = x_train.shape
    model_training_result.point_spread_bet_payouts = point_spread_bet_payouts_decile_masked
    model_training_result.money_line_bet_payouts = money_line_bet_payouts_sum_decile_masked
    model_training_result.bet_at_risk_amount = (bet_size * bet_count)

    ml_net = money_line_bet_payouts_sum_decile_masked - model_training_result.bet_at_risk_amount
    ps_net = point_spread_bet_payouts_decile_masked - model_training_result.bet_at_risk_amount
    max_net = max(ml_net, ps_net)

    model_training_result.roi = max_net / model_training_result.bet_at_risk_amount
    model_training_result.bet_count = bet_count

    if log_to_google_spreadsheet:
        model_training_result.write()
        model_training_result.plot_roi_over_time()

        # money_line_bet_payouts_df = calculate_money_line_bet_payouts_df2(bet_size=bet_size, y_pred=y_pred,
        #                                                                  x_test=x_test, objective=objective)
        # prediction_df = prediction_df.merge(money_line_bet_payouts_df, left_on='game_id', right_on='game_id')
        # model_training_result.log_decile_prediction_distribution(df=prediction_df, decile_colname='decile', correct_colname='is_correct')

    if save_space:
        # https://auto.gluon.ai/tutorials/tabular_prediction/tabular-indepth.html#if-you-encounter-disk-space-issues
        # predictor.save_space()
        # predictor.delete_models(models_to_keep='best', dry_run=False)
        print(f'remove directory tree {trained_models_dir}')
        shutil.rmtree(trained_models_dir)

    return model_training_result
from src.featurizer.team_points_spread_featureizer import calculate_point_spread_bet_payouts_df, \
    calculate_point_spread_bet_payouts_df2


from src.reporting.ModelTrainingResult import ModelTrainingResult


def get_autogluon_predictions(
        input_df,
        feature_columns_list,
        log_to_google_spreadsheet=True,
        model_name=None,
        note='',
        num_trials=None,
        objective=None,
        random_state=44,
        time_limit=60,
        test_size=0.3,
        bet_size=100.0,
):
    """
    :param date_range:
    :param random_state:
    :param time_limit:
    :param test_size:
    :return: ModelTrainingResult
    """
    assert model_name is not None, 'model_name required'
    assert objective is not None, 'objective required'
    for required_feature in feature_columns_list:
        assert input_df[required_feature] is not None, f'feature required: {required_feature}'

    start_time = time.time()
    x = input_df[feature_columns_list]
    y = input_df[objective]
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=test_size, random_state=random_state)

    print(
        'Load training data from DataFrame into an AutoGluon Dataset object. This object is essentially equivalent to a Pandas DataFrame and the same methods can be applied to both.')
    #
    x_train[objective] = y_train
    train_data = task.Dataset(df=x_train)
    # subsample_size = 500  # subsample subset of data for faster demo, try setting this to much larger values
    # train_data = train_data.sample(n=subsample_size, random_state=0)
    print(train_data.head())

    print(
        'Let’s first use these features to predict values, which is recorded in the class column of this table.')

    label_column = objective
    print("Summary of class variable: \n", train_data[label_column].describe())

    print('Now use AutoGluon to train multiple models:')
    dt = datetime.datetime.now()
    trained_models_dir = f'agModels-{model_name}-{label_column}-{dt}'  # specifies folder where to store trained models
    predictor = task.fit(
        eval_metric='accuracy',
        label=label_column,
        num_trials=num_trials,
        output_directory=trained_models_dir,
        time_limits=time_limit,
        train_data=train_data,
        # excluded_model_types=['KNN', 'RF', 'XT']
    )

    print('Next, load separate test data to demonstrate how to make predictions on new examples at inference time:')

    test_data = task.Dataset(df=x_test)
    # y_test = test_data[label_column]  # values to predict
    # test_data_nolab = test_data.drop(labels=[label_column],axis=1)  # delete label column to prove we're not cheating
    print(test_data.head())

    print('We use our trained models to make predictions on the new data and then evaluate performance:')

    # predictor = task.load(dir)  # unnecessary, just demonstrates how to load previously-trained predictor from file

    y_pred = predictor.predict(test_data)
    print("Predictions:  ", y_pred)
    model_performance = predictor.evaluate_predictions(y_true=y_test, y_pred=y_pred, auxiliary_metrics=True)

    # leaderboard = predictor.leaderboard(extra_info=True, silent=True)
    x2_train, x2_test, y2_train, y2_test = train_test_split(input_df, input_df['home.against_the_spread_result'],
                                                            test_size=test_size, random_state=random_state)
    point_spread_bet_payouts = calculate_point_spread_bet_payouts_df(bet_size=bet_size, y_pred=y_pred,
                                                                     x_test=x2_test).sum()

    money_line_bet_payouts = calculate_money_line_bet_payouts_df(bet_size=bet_size, y_pred=y_pred, x_test=x2_test)[
        'money_line_bet_payout']
    money_line_bet_payouts_sum = money_line_bet_payouts.sum()

    model_training_result = ModelTrainingResult()
    model_training_result.model = f'{model_name}-{objective}'
    model_training_result.objective = label_column
    model_training_result.random_seed = random_state
    model_training_result.accuracy = model_performance['accuracy']
    model_training_result.f1_score = model_performance['classification_report']['weighted avg']['f1-score']
    model_training_result.training_seconds = time_limit
    model_training_result.notes = note
    model_training_result.feature_list = x_train.columns.values
    model_training_result.dataset_shape = x.shape
    model_training_result.training_seconds = time.time() - start_time
    model_training_result.point_spread_bet_payouts = point_spread_bet_payouts
    model_training_result.money_line_bet_payouts = money_line_bet_payouts_sum
    model_training_result.bet_at_risk_amount = (bet_size * len(y_pred))
    model_training_result.roi = max(point_spread_bet_payouts, money_line_bet_payouts_sum) / (bet_size * len(y_pred))

    if log_to_google_spreadsheet:
        model_training_result.write()

    # https://auto.gluon.ai/tutorials/tabular_prediction/tabular-indepth.html#if-you-encounter-disk-space-issues
    predictor.save_space()
    predictor.delete_models(models_to_keep='best', dry_run=False)

    return model_training_result
