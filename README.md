```bash
# https://anaconda.org/anaconda/jupyter
conda install -c anaconda jupyter;

# https://www.tutorialspoint.com/jupyter/jupyterlab_installation_and_getting_started.htm
conda install -c conda-forge jupyterlab;
```

# Run

```bash
sh ./venv/bin/activate;
jupyter lab;
```

Then open: [http://localhost:8888/lab]

# PyCharm Config

In Preferences::Tools:Python Integrated Tools, set "Default Test Runner" to pytest.