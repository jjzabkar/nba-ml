from reporting.point_spread_reporter import get_bet_payout


def test_get_bet_payout1():
    result = get_bet_payout(36.0, 'L', -140)
    assert result == -36.0, "expected -100"


def test_get_bet_payout2():
    result = get_bet_payout(100.0, 'P', -140, )
    assert result == 100.0, "expected 100"


def test_get_bet_payout3():
    result = get_bet_payout(36.0, 'W', 175)
    assert result == 63.0, "expected 63"


def test_get_bet_payout4():
    result = get_bet_payout(140.0, 'W', -140)
    assert result == 100.0, "expected 100"


def test_get_bet_payout5():
    result = get_bet_payout(100.0, 'W', 175)
    assert result == 175.0, "expected 175"
