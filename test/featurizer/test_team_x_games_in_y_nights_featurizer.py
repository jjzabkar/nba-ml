import datetime

import pandas as pd
from pandas import DataFrame
from pandas._testing import assert_series_equal

from data_source_services.games.games_service import get_games_featurized_df
from featurizer.team_x_games_in_y_nights_featurizer import get_x_games_in_y_nights_df

df = get_games_featurized_df(season=2017)


def test_get_x_games_in_y_nights_df_columns():
    COLUMN_COUNT = 3
    d = datetime.datetime(year=2016, month=12, day=1).date()
    result = get_x_games_in_y_nights_df(df, season=2017, column_count=COLUMN_COUNT)
    assert result is not None, "expected result"
    assert type(result) is DataFrame, 'expected DataFrame'

    for hv in ['home', 'visitor']:
        for i in range(1, COLUMN_COUNT + 1):
            expected_column = f'{hv}.{i}-nights-ago'
            assert result[expected_column] is not None, f'expected input column {expected_column}'


def test_get_x_games_in_y_nights_df_rest():
    COLUMN_COUNT = 3
    result = get_x_games_in_y_nights_df(df, season=2017, column_count=COLUMN_COUNT)
    row_1 = result[0:1]
    assert row_1['game_id'].values[0] == '2016-10-25-CLE-NYK', 'wrong game, fix games series'
    for hv in ['home', 'visitor']:
        for i in range(1, COLUMN_COUNT + 1):
            col = f'{hv}.{i}-nights-ago'
            assert row_1[col].values[0] == 'R', 'expect R(est) for first game of season'


def test_get_x_games_in_y_nights_df_home_visitor():
    COLUMN_COUNT = 3
    result = get_x_games_in_y_nights_df(df, season=2017, column_count=COLUMN_COUNT)
    row_1 = result[1000:1001]
    assert row_1['game_id'].values[0] == '2017-03-14-NOP-POR', 'wrong game, fix series'

    expected = pd.Series(['2017-03-14-NOP-POR', 'R', 'R', 'V', 'R', 'V', 'H'], index=row_1.columns)
    assert_series_equal(expected, row_1.squeeze(), check_dtype=False, check_names=False)
