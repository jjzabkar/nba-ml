import datetime

from pandas import DataFrame

from featurizer.team_days_since_last_game_featurizer import get_team_days_since_last_game_df


def _get_test_df():
    test_df = DataFrame([
        {'game_id': 0, 'home.abbrev': 'CLE', 'visitor.abbrev': 'GSW', 'date': datetime.date(2016, 6, 1)},
        {'game_id': 1, 'home.abbrev': 'GSW', 'visitor.abbrev': 'CLE', 'date': datetime.date(2016, 6, 2)},
        {'game_id': 2, 'home.abbrev': 'DET', 'visitor.abbrev': 'CLE', 'date': datetime.date(2016, 6, 3)},
        {'game_id': 3, 'home.abbrev': 'MIL', 'visitor.abbrev': 'CLE', 'date': datetime.date(2016, 6, 4)},
        {'game_id': 4, 'home.abbrev': 'GSW', 'visitor.abbrev': 'CLE', 'date': datetime.date(2016, 6, 5)},
        {'game_id': 5, 'home.abbrev': 'MIL', 'visitor.abbrev': 'DET', 'date': datetime.date(2016, 6, 5)},
    ])
    test_df.set_index(['game_id'])
    return test_df


def test_get_team_days_since_last_game_df():
    df = _get_test_df()
    result_df = get_team_days_since_last_game_df(df, 2019)
    assert result_df is not None
    assert len(result_df.columns) == 3, "expected 3 column output DataFrame for featurizer"


def test_get_team_days_since_last_game_df_default():
    df = _get_test_df()
    result_df = get_team_days_since_last_game_df(df, 2019)
    assert result_df['home.days_since_last_game'][0] is None


def test_get_team_days_since_last_game_df_b2b():
    df = _get_test_df()
    result_df = get_team_days_since_last_game_df(df, 2019)
    assert result_df['home.days_since_last_game'][1] == 1


def test_get_team_days_since_last_game_df_more_than_1():
    df = _get_test_df()
    result_df = get_team_days_since_last_game_df(df, 2019)
    assert result_df['home.days_since_last_game'][4] == 3


def test_get_team_days_since_last_game_df_home_and_away():
    df = _get_test_df()
    result_df = get_team_days_since_last_game_df(df, 2019)
    assert result_df['visitor.days_since_last_game'][5] == 2 # previous game was home


def test_get_team_days_since_last_game_df_multiple_games_on_same_day():
    df = _get_test_df()
    result_df = get_team_days_since_last_game_df(df, 2019)
    assert result_df['visitor.days_since_last_game'][4] == 1
    assert result_df['home.days_since_last_game'][5] == 1
