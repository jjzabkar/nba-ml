from pandas import DataFrame

from data_source_services.odds.actionnetwork import get_actionnetwork_moneyline_conversion
from featurizer.money_line_featureizer import _lambda_lookup_moneyline_columns, calculate_money_line_bet_payouts_df, \
    calculate_money_line_bet_payouts_df2


def _get_source_df():
    return DataFrame([
        {'home.abbrev': 'CLE', 'visitor.abbrev': 'GSW', 'home.point_spread': -2.5, },#'winner': 'CLE'},
        {'home.abbrev': 'GSW', 'visitor.abbrev': 'CLE', 'home.point_spread': 2.5, },#'winner': 'CLE'},
        {'home.abbrev': 'DET', 'visitor.abbrev': 'NYK', 'home.point_spread': 0, },#'winner': 'CLE'},
        {'home.abbrev': 'MIL', 'visitor.abbrev': 'SAC', 'home.point_spread': -10, },#'winner': 'CLE'},
    ])


def _get_test_moneline_df():
    x_test = DataFrame(data=
                       {'game_id': [123], 'home.against_the_spread_result': ['W'],
                        'home.point_spread_bet_payout': [-150],
                        'visitor.point_spread_bet_payout': [250], 'home.abbrev': ['CLE'], 'visitor.abbrev': ['GSW'],
                        'home.point_spread': [-2.5], 'winner': ['CLE']})
    x_test.set_index(['game_id'])
    return x_test


def test_lambda_lookup_moneyline_columns():
    ml_conv = get_actionnetwork_moneyline_conversion()
    df_test2 = _get_source_df()
    df_test2['home.money_line'], \
    df_test2['home.money_line_bet_payout'], \
    df_test2['visitor.money_line'], \
    df_test2['visitor.money_line_bet_payout'] = \
        zip(*df_test2['home.point_spread'].apply(
            lambda row: _lambda_lookup_moneyline_columns(row, ml_conv, bet_amount=100.0)))
    assert df_test2.shape == (4, 8)


def test_lambda_lookup_moneyline_columns_home_money_line():
    ml_conv = get_actionnetwork_moneyline_conversion()
    df_test2 = _get_source_df()
    df_test2['home.money_line'], \
    df_test2['home.money_line_bet_payout'], \
    df_test2['visitor.money_line'], \
    df_test2['visitor.money_line_bet_payout'] = \
        zip(*df_test2['home.point_spread'].apply(
            lambda row: _lambda_lookup_moneyline_columns(row, ml_conv, bet_amount=100.0)))
    assert df_test2['home.money_line'][0] == -163
    assert df_test2['home.money_line'][1] == 134
    assert df_test2['home.money_line'][2] == -110
    assert df_test2['home.money_line'][3] == -697


def test_lambda_lookup_moneyline_columns_home_money_line_bet_payout():
    ml_conv = get_actionnetwork_moneyline_conversion()
    df_test2 = _get_source_df()
    df_test2['home.money_line'], \
    df_test2['home.money_line_bet_payout'], \
    df_test2['visitor.money_line'], \
    df_test2['visitor.money_line_bet_payout'] = \
        zip(*df_test2['home.point_spread'].apply(
            lambda row: _lambda_lookup_moneyline_columns(row, ml_conv, bet_amount=100.0)))
    assert df_test2['home.money_line_bet_payout'][0] == 161.35
    assert df_test2['home.money_line_bet_payout'][1] == 234.0
    assert df_test2['home.money_line_bet_payout'][2] == 190.91
    assert df_test2['home.money_line_bet_payout'][3] == 114.35


def test_lambda_lookup_moneyline_columns_visitor_money_line():
    ml_conv = get_actionnetwork_moneyline_conversion()
    df_test2 = _get_source_df()
    df_test2['home.money_line'], \
    df_test2['home.money_line_bet_payout'], \
    df_test2['visitor.money_line'], \
    df_test2['visitor.money_line_bet_payout'] = \
        zip(*df_test2['home.point_spread'].apply(
            lambda row: _lambda_lookup_moneyline_columns(row, ml_conv, bet_amount=100.0)))
    assert df_test2['visitor.money_line'][0] == 134.0
    assert df_test2['visitor.money_line'][1] == -163.0
    assert df_test2['visitor.money_line'][2] == -110.0
    assert df_test2['visitor.money_line'][3] == 478.0


def test_lambda_lookup_moneyline_columns_visitor_money_line_bet_payout():
    ml_conv = get_actionnetwork_moneyline_conversion()
    df_test2 = _get_source_df()
    df_test2['home.money_line'], \
    df_test2['home.money_line_bet_payout'], \
    df_test2['visitor.money_line'], \
    df_test2['visitor.money_line_bet_payout'] = \
        zip(*df_test2['home.point_spread'].apply(
            lambda row: _lambda_lookup_moneyline_columns(row, ml_conv, bet_amount=100.0)))
    assert df_test2['visitor.money_line_bet_payout'][0] == 234.0
    assert df_test2['visitor.money_line_bet_payout'][1] == 161.35
    assert df_test2['visitor.money_line_bet_payout'][2] == 190.91
    assert df_test2['visitor.money_line_bet_payout'][3] == 578.0


def test_calculate_money_line_bet_payouts_win_df():
    y_pred = DataFrame({'y_pred': ['CLE']})
    x_test = _get_test_moneline_df()

    result_df = calculate_money_line_bet_payouts_df2(bet_size=100.0, y_pred=y_pred, x_test=x_test, objective='winner')
    assert result_df is not None

    expected_columns = ['y_pred', 'home.money_line', 'home.money_line_bet_payout', 'visitor.money_line',
                        'visitor.money_line_bet_payout', 'money_line_bet_payout']
    for expected_column in expected_columns:
        assert result_df[expected_column] is not None, f'expected column {expected_column} in output'

    assert result_df['money_line_bet_payout'].sum() == result_df[
        'home.money_line_bet_payout'].sum(), "expected correct money_line_bet_payout"


def test_calculate_money_line_bet_payouts_loss_df():
    y_pred = DataFrame({'y_pred': ['GSW']})
    x_test = _get_test_moneline_df()

    result_df = calculate_money_line_bet_payouts_df2(bet_size=100.0, y_pred=y_pred, x_test=x_test, objective='winner')
    assert result_df is not None

    expected_columns = ['y_pred', 'home.money_line', 'home.money_line_bet_payout', 'visitor.money_line',
                        'visitor.money_line_bet_payout', 'money_line_bet_payout']
    for expected_column in expected_columns:
        assert result_df[expected_column] is not None, f'expected column {expected_column} in output'

    assert result_df['money_line_bet_payout'].sum() == -100.0, "expected correct money_line_bet_payout"
